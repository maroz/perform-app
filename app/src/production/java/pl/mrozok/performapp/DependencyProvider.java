package pl.mrozok.performapp;

final class DependencyProvider {

    private static final String API_BASE_URL = "http://www.mobilefeeds.performgroup.com";

    private DependencyProvider() {
    }

    static DependencyContract getDependency() {
        return new ProductionDependency(API_BASE_URL);
    }
}
