package pl.mrozok.performapp;

import pl.mrozok.performapp.data.WebServiceNewsRepository;
import pl.mrozok.performapp.data.WebServiceScoreRepository;
import pl.mrozok.performapp.data.WebServiceStandingRepository;
import pl.mrozok.performapp.data.repository.NewsRepository;
import pl.mrozok.performapp.data.repository.ScoresRepository;
import pl.mrozok.performapp.data.repository.StandingsRepository;
import pl.mrozok.performapp.webservice.WebServiceContract;
import pl.mrozok.performapp.webservice.WebServiceFactory;

class ProductionDependency implements DependencyContract {

    private final WebServiceContract webService;

    ProductionDependency(String baseWebserviceUrl) {
        webService = WebServiceFactory.create(baseWebserviceUrl);
    }

    @Override
    public NewsRepository provideNewsRepository() {
        return new WebServiceNewsRepository(webService);
    }

    @Override
    public ScoresRepository provideScoresRepository() {
        return new WebServiceScoreRepository(webService);
    }

    @Override
    public StandingsRepository provideStandingsRepository() {
        return new WebServiceStandingRepository(webService);
    }
}
