package pl.mrozok.performapp.data;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import pl.mrozok.performapp.data.repository.StandingsRepository;
import pl.mrozok.performapp.webservice.WebServiceContract;
import pl.mrozok.performapp.webservice.model.Season;
import pl.mrozok.performapp.webservice.model.WebServiceResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebServiceStandingRepository implements StandingsRepository {

    private final WebServiceContract webService;

    public WebServiceStandingRepository(WebServiceContract webService) {
        this.webService = webService;
    }

    @Override
    public void getAll(@NotNull final LoadStandingsCallback callback) {
        Call<WebServiceResponse> call = webService.getStandings();
        call.enqueue(new Callback<WebServiceResponse>() {
            @Override
            public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                if (response.isSuccessful()) {
                    WebServiceResponse webServiceResponse = response.body();
                    callback.onLoaded(getStandingsFromResponse(webServiceResponse));
                } else
                    callback.onLoaded(null);
            }

            @Override
            public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                t.printStackTrace();
                callback.onLoaded(null);
            }
        });
    }

    private static List<Standing> getStandingsFromResponse(WebServiceResponse response) {
        Season season = response.getCompetition().getSeason();
        return season.getRound().getStandings();
    }
}
