package pl.mrozok.performapp.data;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import pl.mrozok.performapp.data.repository.NewsRepository;
import pl.mrozok.performapp.webservice.WebServiceContract;
import pl.mrozok.performapp.webservice.model.GetNewsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebServiceNewsRepository implements NewsRepository {

    private final WebServiceContract webService;

    public WebServiceNewsRepository(WebServiceContract webService) {
        this.webService = webService;
    }

    @Override
    public void getAll(@NotNull final LoadNewsCallback callback) {
        Call<GetNewsResponse> call = webService.getNewsList();
        call.enqueue(new Callback<GetNewsResponse>() {
            @Override
            public void onResponse(Call<GetNewsResponse> call, Response<GetNewsResponse> response) {
                if (response.isSuccessful()) {
                    GetNewsResponse body = response.body();
                    List<News> newsList = body.getChannel().getNewsList();
                    callback.onLoaded(newsList);
                } else
                    callback.onLoaded(null);
            }

            @Override
            public void onFailure(Call<GetNewsResponse> call, Throwable t) {
                t.printStackTrace();
                callback.onLoaded(null);
            }
        });
    }
}
