package pl.mrozok.performapp.data;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.List;

import pl.mrozok.performapp.data.repository.ScoresRepository;
import pl.mrozok.performapp.webservice.WebServiceContract;
import pl.mrozok.performapp.webservice.model.Metadata;
import pl.mrozok.performapp.webservice.model.Season;
import pl.mrozok.performapp.webservice.model.WebServiceResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static pl.mrozok.performapp.utils.DateFormatter.parseStandingsDate;

public class WebServiceScoreRepository implements ScoresRepository {

    private final WebServiceContract webService;

    public WebServiceScoreRepository(WebServiceContract webService) {
        this.webService = webService;
    }

    @Override
    public void getAll(@NotNull final LoadScoresCallback callback) {
        Call<WebServiceResponse> call = webService.getScores();
        call.enqueue(new Callback<WebServiceResponse>() {
            @Override
            public void onResponse(Call<WebServiceResponse> call, Response<WebServiceResponse> response) {
                if (response.isSuccessful()) {
                    WebServiceResponse body = response.body();
                    ScoreTable scoreTable = new ScoreTable(getScoresFromWebServiceResponse(body), findDateInMetadata(body.getMetadata()));
                    callback.onLoaded(scoreTable);
                } else
                    callback.onLoaded(null);
            }

            @Override
            public void onFailure(Call<WebServiceResponse> call, Throwable t) {
                t.printStackTrace();
                callback.onLoaded(null);
            }
        });
    }

    private static Date findDateInMetadata(List<Metadata> metadataSet) {
        for (int i = 0; i < metadataSet.size(); i++) {
            Metadata metadata = metadataSet.get(i);
            if (metadata.getName().equals("date")) {
                return parseStandingsDate(metadata.getValue());
            }
        }
        return null;
    }

    private static List<MatchScore> getScoresFromWebServiceResponse(WebServiceResponse response) {
        Season season = response.getCompetition().getSeason();
        return season.getRound().getScoresFromAllGroups();
    }
}
