package pl.mrozok.performapp.standings;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import pl.mrozok.performapp.data.Standing;
import pl.mrozok.performapp.BR;

public class StandingRowViewModel extends BaseObservable {

    private Standing standing;

    void setStanding(Standing standing) {
        this.standing = standing;
        notifyPropertyChanged(BR.standing);
        notifyPropertyChanged(BR.goalDifference);
    }

    @Bindable
    public Standing getStanding() {
        return standing;
    }

    @Bindable
    public int getGoalDifference() {
        return standing.getScoredGoals() - standing.getGoalsAgainst();
    }
}
