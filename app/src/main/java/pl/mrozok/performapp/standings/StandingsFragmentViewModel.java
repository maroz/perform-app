package pl.mrozok.performapp.standings;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import pl.mrozok.performapp.BR;
import pl.mrozok.performapp.data.Standing;
import pl.mrozok.performapp.data.repository.StandingsRepository;

import static pl.mrozok.performapp.standings.StandingsFragmentViewModelContract.Callbacks;
import static pl.mrozok.performapp.standings.StandingsFragmentViewModelContract.Interactions;
import static pl.mrozok.performapp.utils.AssertionsUtils.checkIsNotNull;

public class StandingsFragmentViewModel extends BaseObservable
        implements Interactions, StandingsRepository.LoadStandingsCallback {

    private Callbacks callbacks;
    private final StandingsRepository standingsRepository;

    private boolean progressBarVisible = true;
    private boolean errorMessageVisible = false;
    private boolean headerVisible = false;

    StandingsFragmentViewModel(@NotNull Callbacks callbacks, @NotNull StandingsRepository standingsRepository) {
        checkIsNotNull(callbacks, "callbacks");
        checkIsNotNull(standingsRepository, "standingsRepository");
        this.callbacks = callbacks;
        this.standingsRepository = standingsRepository;
    }

    @Override
    public void loadStandings() {
        setListHeaderVisibility(false);
        setErrorMessageVisibility(false);
        setProgressBarVisibility(true);
        standingsRepository.getAll(this);
    }

    @Override
    public void onLoaded(List<Standing> standings) {
        setProgressBarVisibility(false);
        if (null == standings)
            setErrorMessageVisibility(true);
        else {
            callbacks.standingsLoaded(standings);
            setListHeaderVisibility(true);
        }
    }

    @Override
    public void setProgressBarVisibility(boolean visible) {
        progressBarVisible = visible;
        notifyPropertyChanged(BR.progressBarVisible);
    }

    @Override
    public void setErrorMessageVisibility(boolean visible) {
        errorMessageVisible = visible;
        notifyPropertyChanged(BR.errorMessageVisible);
    }

    @Override
    public void setListHeaderVisibility(boolean visible) {
        headerVisible = visible;
        notifyPropertyChanged(BR.headerVisible);
    }

    @Bindable
    public boolean isProgressBarVisible() {
        return progressBarVisible;
    }

    @Bindable
    public boolean isErrorMessageVisible() {
        return errorMessageVisible;
    }

    @Bindable
    public boolean isHeaderVisible() {
        return headerVisible;
    }
}
