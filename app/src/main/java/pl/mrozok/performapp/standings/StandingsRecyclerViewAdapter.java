package pl.mrozok.performapp.standings;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.mrozok.performapp.data.Standing;
import pl.mrozok.performapp.databinding.RowStandingBinding;
import pl.mrozok.performapp.standings.StandingsRecyclerViewAdapter.StandingViewHolder;

class StandingsRecyclerViewAdapter extends RecyclerView.Adapter<StandingViewHolder> {

    private final LayoutInflater layoutInflater;
    private List<Standing> standings = new ArrayList<>();

    StandingsRecyclerViewAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public StandingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowStandingBinding rowStandingBinding = RowStandingBinding.inflate(layoutInflater, parent, false);
        rowStandingBinding.setViewModel(new StandingRowViewModel());
        return new StandingViewHolder(rowStandingBinding);
    }

    @Override
    public void onBindViewHolder(StandingViewHolder holder, int position) {
        holder.bindTo(standings.get(position));
    }

    void updateData(List<Standing> standings) {
        this.standings = standings;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return standings.size();
    }

    static class StandingViewHolder extends RecyclerView.ViewHolder {

        private RowStandingBinding binding;

        private StandingViewHolder(RowStandingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bindTo(Standing standing) {
            binding.getViewModel().setStanding(standing);
        }
    }
}
