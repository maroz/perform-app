package pl.mrozok.performapp.standings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pl.mrozok.performapp.BaseFragment;
import pl.mrozok.performapp.data.Standing;
import pl.mrozok.performapp.databinding.FragmentStandingsBinding;
import pl.mrozok.performapp.standings.StandingsFragmentViewModelContract.Callbacks;

public class StandingsFragment extends BaseFragment implements Callbacks {

    private StandingsFragmentViewModel viewModel;
    private FragmentStandingsBinding binding;
    private StandingsRecyclerViewAdapter standingsRecyclerViewAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new StandingsFragmentViewModel(this, app.getStandingsRepository());
        standingsRecyclerViewAdapter = new StandingsRecyclerViewAdapter(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentStandingsBinding.inflate(inflater, container, false);
        binding.setViewModel(viewModel);

        initRecyclerView();
        return binding.getRoot();
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = binding.recyclerView;
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(standingsRecyclerViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.loadStandings();
    }

    @Override
    public void standingsLoaded(List<Standing> standings) {
        standingsRecyclerViewAdapter.updateData(standings);
    }
}
