package pl.mrozok.performapp.standings;

import java.util.List;

import pl.mrozok.performapp.data.Standing;

interface StandingsFragmentViewModelContract {

    interface Interactions {
        void loadStandings();
        
        void setProgressBarVisibility(boolean visible);

        void setErrorMessageVisibility(boolean visible);

        void setListHeaderVisibility(boolean visible);
    }

    interface Callbacks {
        void standingsLoaded(List<Standing> standings);
    }
}
