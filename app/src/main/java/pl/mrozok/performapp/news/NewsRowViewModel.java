package pl.mrozok.performapp.news;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;

import com.android.databinding.library.baseAdapters.BR;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

import pl.mrozok.performapp.data.News;
import pl.mrozok.performapp.utils.DateFormatter;

import static pl.mrozok.performapp.utils.AssertionsUtils.checkIsNotNull;

public class NewsRowViewModel extends BaseObservable {

    private OnNewsClickListener onNewsClickListener;

    private News news;

    NewsRowViewModel(@NotNull OnNewsClickListener onNewsClickListener) {
        checkIsNotNull(onNewsClickListener, "onNewsClickListener");
        this.onNewsClickListener = onNewsClickListener;
    }

    void setNews(News news) {
        this.news = news;
        notifyPropertyChanged(BR.news);
    }

    @Bindable
    public News getNews() {
        return news;
    }

    public String getFormattedPublishDate() {
        Date newsDate = DateFormatter.parseNewsDate(news.getPublishedAt());
        if (null == newsDate)
            return null;
        return DateFormatter.formatDateForNews(newsDate);
    }

    public String getNewsImageUrl() {
        try {
            return news.getEnclosure().getUrl();
        } catch (NullPointerException ignore) {
            return null;
        }
    }

    @SuppressWarnings("unused")
    public void onNewsClick(View view) {
        onNewsClickListener.onNewsClick(news);
    }

    interface OnNewsClickListener {
        void onNewsClick(News news);
    }
}
