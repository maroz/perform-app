package pl.mrozok.performapp.news;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pl.mrozok.performapp.BaseFragment;
import pl.mrozok.performapp.MainActivity;
import pl.mrozok.performapp.data.News;
import pl.mrozok.performapp.databinding.FragmentNewsBinding;
import pl.mrozok.performapp.news.NewsFragmentViewModelContract.Callbacks;

public class NewsFragment extends BaseFragment implements Callbacks, NewsRowViewModel.OnNewsClickListener {

    private FragmentNewsBinding binding;
    private NewsFragmentViewModel viewModel;
    private NewsRecyclerViewAdapter newsRecyclerViewAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new NewsFragmentViewModel(this, app.getNewsRepository());
        newsRecyclerViewAdapter = new NewsRecyclerViewAdapter(getContext(), this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentNewsBinding.inflate(inflater, container, false);
        binding.setViewModel(viewModel);

        initRecyclerView();
        return binding.getRoot();
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = binding.recyclerView;
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(newsRecyclerViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.loadNews();
    }

    @Override
    public void newsLoaded(List<News> newsList) {
        newsRecyclerViewAdapter.updateData(newsList);
    }

    @Override
    public void onNewsClick(News news) {
        Activity activity = getActivity();
        if (null != activity && null != news) {
            ((MainActivity) activity).showNewsDetailsFragment(news.getUrl());
        }
    }
}
