package pl.mrozok.performapp.news;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import pl.mrozok.performapp.data.News;
import pl.mrozok.performapp.data.repository.NewsRepository;
import pl.mrozok.performapp.news.NewsFragmentViewModelContract.Callbacks;
import pl.mrozok.performapp.news.NewsFragmentViewModelContract.Interactions;

import static pl.mrozok.performapp.utils.AssertionsUtils.checkIsNotNull;

public class NewsFragmentViewModel extends BaseObservable implements Interactions, NewsRepository.LoadNewsCallback {

    private Callbacks callbacks;
    @NotNull private final NewsRepository newsRepository;

    private boolean progressBarVisible = true;
    private boolean errorMessageVisible = false;

    NewsFragmentViewModel(@NotNull Callbacks callbacks, @NotNull NewsRepository newsRepository) {
        checkIsNotNull(callbacks, "callbacks");
        checkIsNotNull(newsRepository, "newsRepository");
        this.callbacks = callbacks;
        this.newsRepository = newsRepository;
    }

    @Override
    public void loadNews() {
        setErrorMessageVisibility(false);
        setProgressBarVisibility(true);
        newsRepository.getAll(this);
    }

    @Override
    public void onLoaded(List<News> newsList) {
        setProgressBarVisibility(false);
        if (null == newsList)
            setErrorMessageVisibility(true);
        else
            callbacks.newsLoaded(newsList);
    }

    @Override
    public void setProgressBarVisibility(boolean visible) {
        progressBarVisible = visible;
        notifyPropertyChanged(BR.progressBarVisible);
    }

    @Override
    public void setErrorMessageVisibility(boolean visible) {
        errorMessageVisible = visible;
        notifyPropertyChanged(BR.errorMessageVisible);
    }

    @Bindable
    public boolean isProgressBarVisible() {
        return progressBarVisible;
    }

    @Bindable
    public boolean isErrorMessageVisible() {
        return errorMessageVisible;
    }
}
