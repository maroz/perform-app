package pl.mrozok.performapp.news;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import pl.mrozok.performapp.data.News;
import pl.mrozok.performapp.databinding.RowNewsBinding;
import pl.mrozok.performapp.news.NewsRecyclerViewAdapter.NewsViewHolder;

import static pl.mrozok.performapp.utils.AssertionsUtils.checkIsNotNull;

class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsViewHolder> {

    private final LayoutInflater layoutInflater;
    private final NewsRowViewModel.OnNewsClickListener onNewsClickListener;
    private List<News> newsList = new ArrayList<>();

    NewsRecyclerViewAdapter(@NotNull Context context,
                            @NotNull NewsRowViewModel.OnNewsClickListener onNewsClickListener) {
        checkIsNotNull(context, "context");
        checkIsNotNull(onNewsClickListener, "onNewsClickListener");
        layoutInflater = LayoutInflater.from(context);
        this.onNewsClickListener = onNewsClickListener;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowNewsBinding binding = RowNewsBinding.inflate(layoutInflater, parent, false);
        binding.setViewModel(new NewsRowViewModel(onNewsClickListener));
        return new NewsViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        holder.bindTo(newsList.get(position));
    }

    void updateData(List<News> newsList) {
        this.newsList = newsList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    static class NewsViewHolder extends ViewHolder {

        private RowNewsBinding binding;

        private NewsViewHolder(RowNewsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bindTo(News news) {
            binding.getViewModel().setNews(news);
        }
    }
}
