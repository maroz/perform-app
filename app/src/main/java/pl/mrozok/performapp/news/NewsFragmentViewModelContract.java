package pl.mrozok.performapp.news;

import java.util.List;

import pl.mrozok.performapp.data.News;

interface NewsFragmentViewModelContract {

    interface Interactions {
        void loadNews();

        void setProgressBarVisibility(boolean visible);

        void setErrorMessageVisibility(boolean visible);
    }

    interface Callbacks {
        void newsLoaded(List<News> newsList);
    }
}
