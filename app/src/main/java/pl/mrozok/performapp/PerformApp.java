package pl.mrozok.performapp;

import android.app.Application;

import pl.mrozok.performapp.data.repository.NewsRepository;
import pl.mrozok.performapp.data.repository.ScoresRepository;
import pl.mrozok.performapp.data.repository.StandingsRepository;

public class PerformApp extends Application {

    private NewsRepository newsRepository;
    private ScoresRepository scoresRepository;
    private StandingsRepository standingsRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        DependencyContract dependency = DependencyProvider.getDependency();
        newsRepository = dependency.provideNewsRepository();
        scoresRepository = dependency.provideScoresRepository();
        standingsRepository = dependency.provideStandingsRepository();
    }

    public NewsRepository getNewsRepository() {
        return newsRepository;
    }

    public ScoresRepository getScoresRepository() {
        return scoresRepository;
    }

    public StandingsRepository getStandingsRepository() {
        return standingsRepository;
    }
}
