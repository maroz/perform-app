package pl.mrozok.performapp.data.repository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import pl.mrozok.performapp.data.Standing;

public interface StandingsRepository {

    void getAll(@NotNull LoadStandingsCallback callback);

    interface LoadStandingsCallback {
        void onLoaded(List<Standing> standings);
    }
}
