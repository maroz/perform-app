package pl.mrozok.performapp.data;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root
public class Enclosure {

    @Attribute
    private String type;
    @Attribute
    private String url;
    @Attribute
    private long length;

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public long getLength() {
        return length;
    }
}
