package pl.mrozok.performapp.data;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "item", strict = false)
public class News {

    @Element
    private Enclosure enclosure;

    @Element(name = "pubDate")
    private String publishedAt;

    @Element
    private String title;

    @Element(name = "link")
    private String url;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }
}
