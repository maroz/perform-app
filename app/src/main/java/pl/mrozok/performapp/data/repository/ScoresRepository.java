package pl.mrozok.performapp.data.repository;

import org.jetbrains.annotations.NotNull;

import pl.mrozok.performapp.data.ScoreTable;

public interface ScoresRepository {

    void getAll(@NotNull LoadScoresCallback callback);

    interface LoadScoresCallback {
        void onLoaded(ScoreTable scoreTable);
    }
}
