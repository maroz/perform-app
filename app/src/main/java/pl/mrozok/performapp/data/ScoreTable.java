package pl.mrozok.performapp.data;

import java.util.Date;
import java.util.List;

public class ScoreTable {

    private List<MatchScore> scores;
    private Date date;

    public ScoreTable(List<MatchScore> scores, Date date) {
        this.scores = scores;
        this.date = date;
    }

    public List<MatchScore> getScores() {
        return scores;
    }

    public Date getDate() {
        return date;
    }
}
