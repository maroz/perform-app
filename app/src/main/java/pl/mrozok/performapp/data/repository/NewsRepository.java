package pl.mrozok.performapp.data.repository;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import pl.mrozok.performapp.data.News;

public interface NewsRepository {

    void getAll(@NotNull LoadNewsCallback callback);

    interface LoadNewsCallback {
        void onLoaded(List<News> newsList);
    }
}
