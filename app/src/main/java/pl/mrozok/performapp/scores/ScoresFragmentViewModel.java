package pl.mrozok.performapp.scores;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import org.jetbrains.annotations.NotNull;

import pl.mrozok.performapp.BR;
import pl.mrozok.performapp.data.ScoreTable;
import pl.mrozok.performapp.data.repository.ScoresRepository;
import pl.mrozok.performapp.scores.ScoresFragmentViewModelContract.Callbacks;
import pl.mrozok.performapp.scores.ScoresFragmentViewModelContract.Interactions;
import pl.mrozok.performapp.utils.DateFormatter;
import pl.mrozok.performapp.utils.DelayExecutor;

import static pl.mrozok.performapp.utils.AssertionsUtils.checkIsNotNull;

public class ScoresFragmentViewModel extends BaseObservable implements Interactions,
        ScoresRepository.LoadScoresCallback, DelayExecutor.OnExecuteListener {

    private static final int REFRESH_INTERVAL_IN_SECONDS = 30;

    private final DelayExecutor delayExecutor = new DelayExecutor(this);
    private Callbacks callbacks;
    private ScoresRepository scoresRepository;

    private boolean progressBarVisible = true;
    private boolean errorMessageVisible = false;
    private boolean headerVisible = false;
    private String formattedTitle;

    ScoresFragmentViewModel(@NotNull Callbacks callbacks, @NotNull ScoresRepository scoresRepository) {
        checkIsNotNull(callbacks, "callbacks");
        checkIsNotNull(scoresRepository, "scoresRepository");
        this.callbacks = callbacks;
        this.scoresRepository = scoresRepository;
    }

    @Override
    public void loadScores() {
        setListHeaderVisibility(false);
        setErrorMessageVisibility(false);
        setProgressBarVisibility(true);
        scoresRepository.getAll(this);
    }

    @Override
    public void onLoaded(ScoreTable scoreTable) {
        setProgressBarVisibility(false);
        if (null == scoreTable || null == scoreTable.getScores()) {
            setErrorMessageVisibility(true);
            setFormattedTitle("");
        } else {
            String title = DateFormatter.formatDateForScores(scoreTable.getDate());
            setFormattedTitle(title);
            callbacks.scoresLoaded(scoreTable.getScores());
            setListHeaderVisibility(true);
        }
    }

    @Override
    public void scheduleScoresRefresh() {
        delayExecutor.scheduleWithInterval(REFRESH_INTERVAL_IN_SECONDS);
    }

    @Override
    public void abortSchedule() {
        delayExecutor.abortSchedule();
    }

    @Override
    public void onExecute() {
        loadScores();
    }

    @Override
    public void setProgressBarVisibility(boolean visible) {
        progressBarVisible = visible;
        notifyPropertyChanged(BR.progressBarVisible);
    }

    @Override
    public void setErrorMessageVisibility(boolean visible) {
        errorMessageVisible = visible;
        notifyPropertyChanged(BR.errorMessageVisible);
    }

    @Override
    public void setListHeaderVisibility(boolean visible) {
        headerVisible = visible;
        notifyPropertyChanged(BR.headerVisible);
    }

    @Override
    public void setFormattedTitle(String title) {
        formattedTitle = title;
        notifyPropertyChanged(BR.formattedTitle);
    }

    @Bindable
    public boolean isProgressBarVisible() {
        return progressBarVisible;
    }

    @Bindable
    public boolean isErrorMessageVisible() {
        return errorMessageVisible;
    }

    @Bindable
    public String getFormattedTitle() {
        return formattedTitle;
    }

    @Bindable
    public boolean isHeaderVisible() {
        return headerVisible;
    }
}
