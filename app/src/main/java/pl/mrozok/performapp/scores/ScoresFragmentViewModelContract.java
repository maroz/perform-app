package pl.mrozok.performapp.scores;

import java.util.List;

import pl.mrozok.performapp.data.MatchScore;

interface ScoresFragmentViewModelContract {

    interface Interactions {

        void loadScores();

        void scheduleScoresRefresh();

        void abortSchedule();

        void setProgressBarVisibility(boolean visible);

        void setErrorMessageVisibility(boolean visible);

        void setListHeaderVisibility(boolean visible);

        void setFormattedTitle(String title);
    }

    interface Callbacks {
        void scoresLoaded(List<MatchScore> scores);
    }
}
