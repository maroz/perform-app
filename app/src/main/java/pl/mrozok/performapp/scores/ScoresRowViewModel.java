package pl.mrozok.performapp.scores;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import pl.mrozok.performapp.data.MatchScore;
import pl.mrozok.performapp.BR;

public class ScoresRowViewModel extends BaseObservable {

    private static final String SCORE_FORMAT = "%s - %s";

    private MatchScore matchScore;

    @Bindable
    public MatchScore getMatchScore() {
        return matchScore;
    }

    public void setMatchScore(MatchScore matchScore) {
        this.matchScore = matchScore;
        notifyPropertyChanged(BR.matchScore);
        notifyPropertyChanged(BR.formattedScore);
    }

    @Bindable
    public String getFormattedScore() {
        return String.format(SCORE_FORMAT, matchScore.getHomeTeamScore(),
                matchScore.getAwayTeamScore());
    }
}
