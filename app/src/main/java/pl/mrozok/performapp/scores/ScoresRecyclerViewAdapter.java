package pl.mrozok.performapp.scores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.mrozok.performapp.data.MatchScore;
import pl.mrozok.performapp.databinding.RowScoreBinding;
import pl.mrozok.performapp.scores.ScoresRecyclerViewAdapter.ScoresViewHolder;

class ScoresRecyclerViewAdapter extends RecyclerView.Adapter<ScoresViewHolder> {

    private List<MatchScore> scores = new ArrayList<>();

    private final LayoutInflater layoutInflater;

    ScoresRecyclerViewAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ScoresViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RowScoreBinding binding = RowScoreBinding.inflate(layoutInflater, parent, false);
        binding.setViewModel(new ScoresRowViewModel());
        return new ScoresViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ScoresViewHolder holder, int position) {
        holder.bindTo(scores.get(position));
    }

    void updateData(List<MatchScore> scores) {
        this.scores = scores;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return scores.size();
    }

    static class ScoresViewHolder extends RecyclerView.ViewHolder {

        private RowScoreBinding binding;

        private ScoresViewHolder(RowScoreBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bindTo(MatchScore score) {
            binding.getViewModel().setMatchScore(score);
        }
    }
}
