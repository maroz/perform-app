package pl.mrozok.performapp.scores;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import pl.mrozok.performapp.BaseFragment;
import pl.mrozok.performapp.data.MatchScore;
import pl.mrozok.performapp.databinding.FragmentScoresBinding;
import pl.mrozok.performapp.scores.ScoresFragmentViewModelContract.Callbacks;

public class ScoresFragment extends BaseFragment implements Callbacks {
    
    private FragmentScoresBinding binding;
    private ScoresFragmentViewModel viewModel;
    private ScoresRecyclerViewAdapter scoresRecyclerViewAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ScoresFragmentViewModel(this, app.getScoresRepository());
        scoresRecyclerViewAdapter = new ScoresRecyclerViewAdapter(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentScoresBinding.inflate(inflater, container, false);
        binding.setViewModel(viewModel);

        initRecyclerView();
        return binding.getRoot();
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = binding.recyclerView;
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(scoresRecyclerViewAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.loadScores();
        viewModel.scheduleScoresRefresh();
    }

    @Override
    public void onPause() {
        viewModel.abortSchedule();
        super.onPause();
    }

    @Override
    public void scoresLoaded(List<MatchScore> scores) {
        scoresRecyclerViewAdapter.updateData(scores);
    }
}
