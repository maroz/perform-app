package pl.mrozok.performapp;

import android.content.Context;
import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {

    protected PerformApp app;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        app = getPerformApp();
    }

    private PerformApp getPerformApp() {
        return (PerformApp) getActivity().getApplication();
    }
}
