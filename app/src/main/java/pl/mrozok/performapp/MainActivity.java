package pl.mrozok.performapp;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import pl.mrozok.performapp.databinding.ActivityMainBinding;
import pl.mrozok.performapp.news.NewsFragment;
import pl.mrozok.performapp.newsdetails.NewsDetailsFragment;
import pl.mrozok.performapp.scores.ScoresFragment;
import pl.mrozok.performapp.standings.StandingsFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initBinding();
        showFragment(new NewsFragment(), R.string.news);
    }

    private void initBinding() {
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.toolbar);
    }

    private void showFragment(Fragment fragment, @StringRes int title) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
        setTitle(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.actionNews:
                showFragment(new NewsFragment(), R.string.news);
                return true;

            case R.id.actionScores:
                showFragment(new ScoresFragment(), R.string.scores);
                return true;

            case R.id.actionStandings:
                showFragment(new StandingsFragment(), R.string.standings);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showNewsDetailsFragment(String url) {
        if (null != url) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, NewsDetailsFragment.createForUrl(url))
                    .addToBackStack(null)
                    .commit();
        }
    }
}
