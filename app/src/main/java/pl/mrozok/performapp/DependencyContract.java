package pl.mrozok.performapp;

import pl.mrozok.performapp.data.repository.NewsRepository;
import pl.mrozok.performapp.data.repository.ScoresRepository;
import pl.mrozok.performapp.data.repository.StandingsRepository;

interface DependencyContract {

    NewsRepository provideNewsRepository();

    ScoresRepository provideScoresRepository();

    StandingsRepository provideStandingsRepository();
}
