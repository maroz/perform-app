package pl.mrozok.performapp.webservice;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class WebServiceFactory {

    public static WebServiceContract create(String baseUrl) {
        OkHttpClient httpClient = new OkHttpClient.Builder().build();

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient)
                .addConverterFactory(SimpleXmlConverterFactory.create());

        return retrofitBuilder.client(httpClient)
                .build()
                .create(WebServiceContract.class);
    }
}
