package pl.mrozok.performapp.webservice.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

import pl.mrozok.performapp.data.News;

@Root(strict = false)
public class Channel {

    @ElementList(inline = true)
    private List<News> newsList;

    public List<News> getNewsList() {
        return newsList;
    }
}
