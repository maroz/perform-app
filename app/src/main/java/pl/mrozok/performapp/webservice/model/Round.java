package pl.mrozok.performapp.webservice.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

import pl.mrozok.performapp.data.MatchScore;
import pl.mrozok.performapp.data.Standing;

@Root(strict = false)
public class Round {

    @ElementList(inline = true, required = false)
    private List<Group> groups;

    @ElementList(name = "resultstable", required = false)
    private List<Standing> standings;

    public List<Group> getGroups() {
        return groups;
    }

    public List<Standing> getStandings() {
        return standings;
    }

    public List<MatchScore> getScoresFromAllGroups() {
        List<MatchScore> scores = new ArrayList<>();
        for (int i = 0; i < groups.size(); i++) {
            Group group = groups.get(i);
            scores.addAll(group.getScores());
        }
        return scores;
    }
}
