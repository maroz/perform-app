package pl.mrozok.performapp.webservice.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

import pl.mrozok.performapp.data.MatchScore;

@Root(name = "group", strict = false)
public class Group {

    @ElementList(inline = true)
    private List<MatchScore> scores;

    List<MatchScore> getScores() {
        return scores;
    }
}
