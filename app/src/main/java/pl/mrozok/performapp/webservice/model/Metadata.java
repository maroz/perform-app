package pl.mrozok.performapp.webservice.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "parameter", strict = false)
public class Metadata {

    @Attribute
    private String name;

    @Attribute
    private String value;

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
