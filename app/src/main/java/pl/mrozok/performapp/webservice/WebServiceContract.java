package pl.mrozok.performapp.webservice;

import pl.mrozok.performapp.webservice.model.GetNewsResponse;
import pl.mrozok.performapp.webservice.model.WebServiceResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface WebServiceContract {

    @GET("/utilities/interviews/techtest/scores.xml")
    Call<WebServiceResponse> getScores();

    @GET("/utilities/interviews/techtest/standings.xml")
    Call<WebServiceResponse> getStandings();

    @GET("/utilities/interviews/techtest/latestnews.xml")
    Call<GetNewsResponse> getNewsList();
}
