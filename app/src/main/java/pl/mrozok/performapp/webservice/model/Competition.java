package pl.mrozok.performapp.webservice.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class Competition {

    @Element
    private Season season;

    public Season getSeason() {
        return season;
    }
}
