package pl.mrozok.performapp.webservice.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "gsmrs", strict = false)
public class WebServiceResponse {

    @ElementList(name = "method")
    private List<Metadata> metadata;

    @Element
    private Competition competition;

    public List<Metadata> getMetadata() {
        return metadata;
    }

    public Competition getCompetition() {
        return competition;
    }
}
