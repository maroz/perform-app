package pl.mrozok.performapp.webservice.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(strict = false)
public class Season {

    @Element
    private Round round;

    public Round getRound() {
        return round;
    }
}
