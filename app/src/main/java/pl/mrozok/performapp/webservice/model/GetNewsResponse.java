package pl.mrozok.performapp.webservice.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "rss", strict = false)
public class GetNewsResponse {

    @Element
    private Channel channel;

    public Channel getChannel() {
        return channel;
    }
}
