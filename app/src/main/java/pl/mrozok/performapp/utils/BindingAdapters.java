package pl.mrozok.performapp.utils;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class BindingAdapters {

    @BindingAdapter("srcGlide")
    public static void loadImageWithGlide(ImageView view, String url) {
        Context context = view.getContext();
        if (null == context || null == url)
            return;
        Glide.with(context)
                .load(url)
                .into(view);
    }
}
