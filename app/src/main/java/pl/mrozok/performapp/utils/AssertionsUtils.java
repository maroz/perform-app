package pl.mrozok.performapp.utils;

public class AssertionsUtils {

    private AssertionsUtils() {
    }

    public static void checkIsNotNull(Object object, String objectName) {
        if (null == object)
            throw new IllegalArgumentException("null ".concat(objectName));
    }
}
