package pl.mrozok.performapp.utils;

import android.os.Handler;
import android.os.HandlerThread;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import static pl.mrozok.performapp.utils.AssertionsUtils.checkIsNotNull;

public class DelayExecutor implements Runnable {

    private static final String TAG = DelayExecutor.class.getSimpleName();

    private final OnExecuteListener onExecuteListener;
    private HandlerThread thread;
    private Handler handler;

    public DelayExecutor(@NotNull OnExecuteListener onExecuteListener) {
        checkIsNotNull(onExecuteListener, "onExecuteListener");
        this.onExecuteListener = onExecuteListener;
    }

    public void scheduleWithInterval(int intervalInSeconds) {
        thread = new HandlerThread(TAG);
        thread.start();
        handler = new Handler(thread.getLooper());
        long millisInterval = TimeUnit.SECONDS.toMillis(intervalInSeconds);
        handler.postDelayed(this, millisInterval);
    }

    @Override
    public void run() {
        onExecuteListener.onExecute();
    }

    public void abortSchedule() {
        handler.removeCallbacks(null);
        thread.quit();
    }

    public interface OnExecuteListener {
        void onExecute();
    }
}
