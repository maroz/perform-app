package pl.mrozok.performapp.utils;

import android.support.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {

    private static final String NEWS_DATE_FORMAT_PATTERN = "EEEE, d MMMM yyyy, HH:mm";
    private static final String SCORES_DATE_FORMAT_PATTERN = "d MMMM yyyy";

    private static final SimpleDateFormat STANDINGS_DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static final SimpleDateFormat NEWS_DATE_FORMAT =
            new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);

    public static Date parseNewsDate(String date) {
        try {
            return NEWS_DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String formatDateForNews(Date date) {
        return formatDateForNews(Locale.getDefault(), date);
    }

    static String formatDateForNews(Locale locale, Date date) {
        return new SimpleDateFormat(NEWS_DATE_FORMAT_PATTERN, locale).format(date);
    }

    public static String formatDateForScores(Date date) {
        return formatDateForScores(Locale.getDefault(), date);
    }

    static String formatDateForScores(Locale locale, Date date) {
        return new SimpleDateFormat(SCORES_DATE_FORMAT_PATTERN, locale).format(date);
    }

    @Nullable
    public static Date parseStandingsDate(String date) {
        try {
            return STANDINGS_DATE_FORMAT.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }
}
