package pl.mrozok.performapp.newsdetails;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

public class NewsDetailsViewModel extends BaseObservable implements NewsDetailsWebViewClient.OnPageLoadFinishListener {

    private final NewsDetailsWebViewClient webViewClient = new NewsDetailsWebViewClient(this);

    private boolean pageLoading = true;

    @Override
    public void onPageLoadFinish() {
        setPageLoading(false);
    }

    @Bindable
    public boolean isPageLoading() {
        return pageLoading;
    }

    private void setPageLoading(boolean pageLoading) {
        this.pageLoading = pageLoading;
        notifyPropertyChanged(BR.pageLoading);
    }

    NewsDetailsWebViewClient getWebViewClient() {
        return webViewClient;
    }
}
