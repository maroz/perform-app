package pl.mrozok.performapp.newsdetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import pl.mrozok.performapp.BaseFragment;
import pl.mrozok.performapp.databinding.FragmentNewsDetailsBinding;

import static pl.mrozok.performapp.utils.AssertionsUtils.checkIsNotNull;

public class NewsDetailsFragment extends BaseFragment {

    private static final String EXTRA_URL = "url";

    private FragmentNewsDetailsBinding binding;
    private NewsDetailsViewModel viewModel;

    public static NewsDetailsFragment createForUrl(String url) {
        checkIsNotNull(url, "url");
        NewsDetailsFragment fragment = new NewsDetailsFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new NewsDetailsViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentNewsDetailsBinding.inflate(inflater, container, false);
        binding.setViewModel(viewModel);
        setUpWebView(binding.webContent);

        handleArgs(getArguments());

        return binding.getRoot();
    }

    private void setUpWebView(WebView webContent) {
        webContent.setWebViewClient(viewModel.getWebViewClient());
        WebSettings settings = webContent.getSettings();
        settings.setBuiltInZoomControls(true);
    }

    private void handleArgs(Bundle arguments) {
        if (null != arguments) {
            String url = arguments.getString(EXTRA_URL);
            binding.webContent.loadUrl(url);
        }
    }
}
