package pl.mrozok.performapp.newsdetails;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.jetbrains.annotations.NotNull;

import static pl.mrozok.performapp.utils.AssertionsUtils.checkIsNotNull;

class NewsDetailsWebViewClient extends WebViewClient {

    private final OnPageLoadFinishListener onPageLoadFinishListener;

    public NewsDetailsWebViewClient(@NotNull OnPageLoadFinishListener onPageLoadFinishListener) {
        checkIsNotNull(onPageLoadFinishListener, "onPageLoadFinishListener");
        this.onPageLoadFinishListener = onPageLoadFinishListener;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        onPageLoadFinishListener.onPageLoadFinish();
    }

    interface OnPageLoadFinishListener {
        void onPageLoadFinish();
    }
}
