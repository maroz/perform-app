package pl.mrozok.performapp.standings;

import org.junit.Before;
import org.junit.Test;

import pl.mrozok.performapp.data.Standing;

import static org.junit.Assert.*;

public class StandingRowViewModelTest {

    private StandingRowViewModel viewModel;

    @Before
    public void setUp() {
        viewModel = new StandingRowViewModel();
    }

    @Test
    public void getGoalDifferenceShouldReturnScoredGoalsReducedByGoalsAgainst() {
        Standing standing = new Standing();
        standing.setScoredGoals(25);
        standing.setGoalsAgainst(10);

        viewModel.setStanding(standing);
        assertEquals(25 - 10, viewModel.getGoalDifference());
    }
}
