package pl.mrozok.performapp.standings;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.List;

import pl.mrozok.performapp.data.Standing;
import pl.mrozok.performapp.data.repository.StandingsRepository;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class StandingsFragmentViewModelTest {

    private static final List<Standing> STANDINGS = Lists.newArrayList(new Standing(), new Standing());
    private StandingsFragmentViewModel viewModel;

    @Mock
    private StandingsRepository standingsRepository;
    @Mock
    private StandingsFragmentViewModelContract.Callbacks viewModelCallbacks;
    @Captor
    private ArgumentCaptor<StandingsRepository.LoadStandingsCallback> loadStandingCallback;

    @Before
    public void setUp() {
        initMocks(this);
        viewModel = spy(new StandingsFragmentViewModel(viewModelCallbacks, standingsRepository));
    }

    @Test
    public void loadStandingsShouldLoadStandingsIntoView() {
        viewModel.loadStandings();
        verify(viewModel).setListHeaderVisibility(false);
        verify(viewModel).setErrorMessageVisibility(false);
        verify(viewModel).setProgressBarVisibility(true);

        verify(standingsRepository).getAll(loadStandingCallback.capture());
        loadStandingCallback.getValue().onLoaded(STANDINGS);

        verify(viewModel).setProgressBarVisibility(false);
        verify(viewModel).setListHeaderVisibility(true);
        verify(viewModelCallbacks).standingsLoaded(STANDINGS);
    }

    @Test
    public void loadStandingsShouldDisplayErrorMessageWhenDataIsNull() {
        viewModel.loadStandings();
        verify(viewModel).setListHeaderVisibility(false);
        verify(viewModel).setErrorMessageVisibility(false);
        verify(viewModel).setProgressBarVisibility(true);

        verify(standingsRepository).getAll(loadStandingCallback.capture());
        loadStandingCallback.getValue().onLoaded(null);

        verify(viewModel).setProgressBarVisibility(false);
        verify(viewModel).setErrorMessageVisibility(true);
    }
}
