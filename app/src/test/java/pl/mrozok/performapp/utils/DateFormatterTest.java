package pl.mrozok.performapp.utils;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.getInstance;
import static org.junit.Assert.assertEquals;

public class DateFormatterTest {

    @Test
    public void parseNewsDate() {
        Date date = DateFormatter.parseNewsDate("Tue, 01 Jan 2013 16:00:00 +0000");
        Calendar calendar = getInstance();
        calendar.setTime(date);
        assertEquals(2013, calendar.get(YEAR));
        assertEquals(0, calendar.get(MONTH));
        assertEquals(1, calendar.get(DAY_OF_MONTH));
        assertEquals(17, calendar.get(HOUR_OF_DAY));
        assertEquals(0, calendar.get(MINUTE));
    }

    @Test
    public void formatDateForNewsShouldReturnDateInProperFormat() {
        String expected = "Wednesday, 7 December 2011, 13:35";
        Calendar calendar = Calendar.getInstance();
        calendar.set(YEAR, 2011);
        calendar.set(DAY_OF_MONTH, 7);
        calendar.set(MINUTE, 35);
        calendar.set(HOUR_OF_DAY, 13);
        calendar.set(MONTH, 11);
        Date date = calendar.getTime();

        String output = DateFormatter.formatDateForNews(Locale.ENGLISH, date);

        assertEquals(expected, output);
    }

    @Test
    public void formatDateForScoresShouldReturnDateInProperFormat() {
        String expected = "17 April 2013";
        Calendar calendar = Calendar.getInstance();
        calendar.set(YEAR, 2013);
        calendar.set(DAY_OF_MONTH, 17);
        calendar.set(MONTH, 3);
        Date date = calendar.getTime();

        String output = DateFormatter.formatDateForScores(Locale.ENGLISH, date);

        assertEquals(expected, output);
    }

    @Test
    public void parseStandingsDateShouldParseDateFromStandingWebServiceResponse() {
        String input = "2012-09-19";
        Date date = DateFormatter.parseStandingsDate(input);
        Calendar calendar = getInstance();
        calendar.setTime(date);

        assertEquals(2012, calendar.get(YEAR));
        assertEquals(8, calendar.get(MONTH));
        assertEquals(19, calendar.get(DAY_OF_MONTH));
    }
}
