package pl.mrozok.performapp.utils;

import org.junit.Test;

public class AssertionsUtilsTest {

    @Test(expected = IllegalArgumentException.class)
    public void checkIsNotNullShouldThrowExceptionWhenObjectIsNull() {
        AssertionsUtils.checkIsNotNull(null, "test object");
    }

}