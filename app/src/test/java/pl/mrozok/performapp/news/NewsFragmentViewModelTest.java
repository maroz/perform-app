package pl.mrozok.performapp.news;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.List;

import pl.mrozok.performapp.data.News;
import pl.mrozok.performapp.data.repository.NewsRepository;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class NewsFragmentViewModelTest {

    private static final List<News> NEWS = Lists.newArrayList(new News(), new News());
    private NewsFragmentViewModel viewModel;

    @Mock
    private NewsRepository newsRepository;
    @Mock
    private NewsFragmentViewModelContract.Callbacks viewModelCallbacks;
    @Captor
    private ArgumentCaptor<NewsRepository.LoadNewsCallback> loadNewsCallback;

    @Before
    public void setUp() {
        initMocks(this);
        viewModel = spy(new NewsFragmentViewModel(viewModelCallbacks, newsRepository));
    }

    @Test
    public void loadNewsShouldLoadNewsIntoView() {
        viewModel.loadNews();
        verify(viewModel).setErrorMessageVisibility(false);
        verify(viewModel).setProgressBarVisibility(true);

        verify(newsRepository).getAll(loadNewsCallback.capture());
        loadNewsCallback.getValue().onLoaded(NEWS);

        verify(viewModel).setProgressBarVisibility(false);
        verify(viewModelCallbacks).newsLoaded(NEWS);
    }

    @Test
    public void loadNewsShouldDisplayErrorMessageWhenDataIsNull() {
        viewModel.loadNews();
        verify(viewModel).setErrorMessageVisibility(false);
        verify(viewModel).setProgressBarVisibility(true);

        verify(newsRepository).getAll(loadNewsCallback.capture());
        loadNewsCallback.getValue().onLoaded(null);

        verify(viewModel).setProgressBarVisibility(false);
        verify(viewModel).setErrorMessageVisibility(true);
    }
}
