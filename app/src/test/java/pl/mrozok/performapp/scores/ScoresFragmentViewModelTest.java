package pl.mrozok.performapp.scores;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.Date;

import pl.mrozok.performapp.data.MatchScore;
import pl.mrozok.performapp.data.ScoreTable;
import pl.mrozok.performapp.data.repository.ScoresRepository;
import pl.mrozok.performapp.utils.DateFormatter;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class ScoresFragmentViewModelTest {

    private static final ScoreTable SCORE_TABLE =
            new ScoreTable(Lists.newArrayList(createMatch("Team 1", "Team 2", 1, 1),
                    createMatch("Team 3", "Team 4", 2, 0)), new Date());
    private ScoresFragmentViewModel viewModel;

    @Mock
    private ScoresFragmentViewModelContract.Callbacks viewModelCallbacks;
    @Mock
    private ScoresRepository scoresRepository;

    @Captor
    private ArgumentCaptor<ScoresRepository.LoadScoresCallback> loadScoresCallback;

    private static MatchScore createMatch(String homeTeam, String awayTeam,
                                          int homeTeamScore, int awayTeamScore) {
        MatchScore matchScore = new MatchScore();
        matchScore.setHomeTeam(homeTeam);
        matchScore.setHomeTeamScore(homeTeamScore);
        matchScore.setAwayTeam(awayTeam);
        matchScore.setAwayTeamScore(awayTeamScore);
        return matchScore;
    }

    @Before
    public void setUp() {
        initMocks(this);

        viewModel = spy(new ScoresFragmentViewModel(viewModelCallbacks, scoresRepository));
    }

    @Test
    public void loadScoresShouldLoadScoresIntoView() {
        viewModel.loadScores();
        verify(viewModel).setListHeaderVisibility(false);
        verify(viewModel).setErrorMessageVisibility(false);
        verify(viewModel).setProgressBarVisibility(true);

        verify(scoresRepository).getAll(loadScoresCallback.capture());
        loadScoresCallback.getValue().onLoaded(SCORE_TABLE);

        verify(viewModel).setProgressBarVisibility(false);
        verify(viewModel).setListHeaderVisibility(true);
        String expectedTitle = DateFormatter.formatDateForScores(SCORE_TABLE.getDate());
        verify(viewModel).setFormattedTitle(expectedTitle);
        verify(viewModelCallbacks).scoresLoaded(SCORE_TABLE.getScores());
    }

    @Test
    public void loadScoresShouldDisplayErrorMessageWhenDataIsNull() {
        viewModel.loadScores();
        verify(viewModel).setListHeaderVisibility(false);
        verify(viewModel).setErrorMessageVisibility(false);
        verify(viewModel).setProgressBarVisibility(true);

        verify(scoresRepository).getAll(loadScoresCallback.capture());
        loadScoresCallback.getValue().onLoaded(null);

        verify(viewModel).setFormattedTitle("");
        verify(viewModel).setProgressBarVisibility(false);
        verify(viewModel).setErrorMessageVisibility(true);
    }
}