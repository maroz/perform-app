package pl.mrozok.performapp.scores;

import org.junit.Before;
import org.junit.Test;

import pl.mrozok.performapp.data.MatchScore;

import static org.junit.Assert.assertEquals;

public class ScoresRowViewModelTest {

    private ScoresRowViewModel scoresRowPresenter;

    @Before
    public void setUp() {
        scoresRowPresenter = new ScoresRowViewModel();
    }

    @Test
    public void formatScoreShouldReturnProperFormattedScore() {
        String expectedOutput = "2 - 1";

        MatchScore matchScore = new MatchScore();
        matchScore.setHomeTeamScore(2);
        matchScore.setAwayTeamScore(1);
        scoresRowPresenter.setMatchScore(matchScore);

        assertEquals(expectedOutput, scoresRowPresenter.getFormattedScore());
    }

}