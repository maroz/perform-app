package pl.mrozok.performapp.data.repository;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.mrozok.performapp.data.Standing;

public class InMemoryStandingsRepository implements StandingsRepository {

    private static final List<Standing> STANDINGS = new ArrayList<>();

    public InMemoryStandingsRepository() {
        init();
    }

    private void init() {
        STANDINGS.add(randomStanding(1));
        STANDINGS.add(randomStanding(2));
        STANDINGS.add(randomStanding(3));
        STANDINGS.add(randomStanding(4));
    }

    private static Standing randomStanding(int id) {
        Random random = new Random();
        Standing standing = new Standing();
        standing.setClubName("Football club " + id);
        standing.setRank(id);
        standing.setMatchesDraw(random.nextInt(10));
        standing.setMatchesWon(random.nextInt(10));
        standing.setMatchesLost(random.nextInt(10));
        standing.setPoints(random.nextInt(30));
        return standing;
    }

    @Override
    public void getAll(@NotNull LoadStandingsCallback callback) {
        callback.onLoaded(STANDINGS);
    }
}
