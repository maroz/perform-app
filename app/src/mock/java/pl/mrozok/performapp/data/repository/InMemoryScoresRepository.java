package pl.mrozok.performapp.data.repository;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.mrozok.performapp.data.MatchScore;
import pl.mrozok.performapp.data.ScoreTable;

public class InMemoryScoresRepository implements ScoresRepository {

    private static final ScoreTable SCORE_TABLE = new ScoreTable(new ArrayList<MatchScore>(), new Date());

    public InMemoryScoresRepository() {
        init();
    }

    private void init() {
        List<MatchScore> scores = SCORE_TABLE.getScores();
        scores.add(createMatch("Team 1", "Team 2", 1, 1));
        scores.add(createMatch("Team 3", "Team 4", 2, 0));
    }

    private static MatchScore createMatch(String homeTeam, String awayTeam,
                                          int homeTeamScore, int awayTeamScore) {
        MatchScore matchScore = new MatchScore();
        matchScore.setHomeTeam(homeTeam);
        matchScore.setHomeTeamScore(homeTeamScore);
        matchScore.setAwayTeam(awayTeam);
        matchScore.setAwayTeamScore(awayTeamScore);
        return matchScore;
    }

    @Override
    public void getAll(@NotNull LoadScoresCallback callback) {
        callback.onLoaded(SCORE_TABLE);
    }
}
