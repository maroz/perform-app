package pl.mrozok.performapp.data.repository;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import pl.mrozok.performapp.data.News;

public class InMemoryNewsRepository implements NewsRepository {

    private static final List<News> NEWS_LIST = new ArrayList<>();

    public InMemoryNewsRepository() {
        init();
    }

    private void init() {
        NEWS_LIST.add(createNews(1));
        NEWS_LIST.add(createNews(2));
        NEWS_LIST.add(createNews(3));
        NEWS_LIST.add(createNews(4));
    }

    private static News createNews(int id) {
        News news = new News();
        news.setTitle("Title " + id);
        news.setPublishedAt("Tue, 01 Jan 2013 17:00:00 +0000");
        return news;
    }

    @Override
    public void getAll(@NotNull LoadNewsCallback callback) {
        callback.onLoaded(NEWS_LIST);
    }
}
