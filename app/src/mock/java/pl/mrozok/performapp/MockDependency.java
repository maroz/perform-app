package pl.mrozok.performapp;

import pl.mrozok.performapp.data.repository.InMemoryNewsRepository;
import pl.mrozok.performapp.data.repository.InMemoryScoresRepository;
import pl.mrozok.performapp.data.repository.InMemoryStandingsRepository;
import pl.mrozok.performapp.data.repository.NewsRepository;
import pl.mrozok.performapp.data.repository.ScoresRepository;
import pl.mrozok.performapp.data.repository.StandingsRepository;

class MockDependency implements DependencyContract {

    @Override
    public NewsRepository provideNewsRepository() {
        return new InMemoryNewsRepository();
    }

    @Override
    public ScoresRepository provideScoresRepository() {
        return new InMemoryScoresRepository();
    }

    @Override
    public StandingsRepository provideStandingsRepository() {
        return new InMemoryStandingsRepository();
    }
}
