package pl.mrozok.performapp;

final class DependencyProvider {

    private DependencyProvider() {
    }

    static DependencyContract getDependency() {
        return new MockDependency();
    }
}
